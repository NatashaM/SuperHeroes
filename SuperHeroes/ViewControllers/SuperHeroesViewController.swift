//
//  SuperHeroesViewController.swift
//  SuperHeroes
//
//  Created by Наталья Миронова on 25.05.2023.
//

import UIKit

//MARK: - Super Heroes ViewController
class SuperHeroesViewController: UIViewController {
    
    //MARK: - Private property
    private var superheroes: [Superhero] = []
    
    private let searchController = UISearchController(searchResultsController: nil)
    private var filteredHero: [Superhero] = []
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    private var collectionView: UICollectionView!
    
    //MARK: - Override UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        fetchSuperheroes()
    }
    
    private func fetchSuperheroes() {
        NetworkManager.shared.fetchData { result in
            switch result {
            case .success(let superheroes):
                self.superheroes = superheroes
                self.collectionView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension SuperHeroesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        isFiltering ? filteredHero.count : superheroes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier,
                                                      for: indexPath) as! CollectionViewCell
        let superhero =  isFiltering ? filteredHero[indexPath.row] : superheroes[indexPath.row]
        print(superhero)
        cell.configure(with: superhero)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let superhero = isFiltering ? filteredHero[indexPath.row] : superheroes[indexPath.row]
        let infoVC = SuperheroInfoViewController()
        infoVC.superhero = superhero
        navigationController?.pushViewController(infoVC, animated: true)
    }
}

//MARK: - Setting View
private extension SuperHeroesViewController {
    func setupView() {
        
        setupCollectionView()
        addSubViews()
        setupSearchHero()
        setupNavigationController()
        
    }
}

//MARK: - Setting
private extension SuperHeroesViewController {
    
    func addSubViews() {
        view.addSubview(collectionView)
    }
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 32
        layout.sectionInset.right = 16
        layout.sectionInset.left = 16
        layout.itemSize = CGSize(width: (view.frame.size.width/2) - 32,
                                 height: (view.frame.size.height/3) - 16)
        collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: layout)
        collectionView.backgroundColor = .black
        collectionView.register(CollectionViewCell.self,
                                forCellWithReuseIdentifier: CollectionViewCell.identifier)
        collectionView.frame = view.bounds
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    func setupSearchHero() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.barTintColor = .white
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        if let textField = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            textField.font = UIFont.boldSystemFont(ofSize: 17)
            textField.textColor = .white
        }
    }
    
    func setupNavigationController() {
        title = "Super Heroes"
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.green]
    }
}

// MARK: - UISearchResultsUpdating
extension SuperHeroesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    private func filterContentForSearchText(_ searchText: String) {
        filteredHero = superheroes.filter { hero in
            hero.name.lowercased().contains(searchText.lowercased())
        } 
        collectionView.reloadData()
    }
}

