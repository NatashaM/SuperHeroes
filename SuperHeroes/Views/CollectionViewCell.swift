//
//  CollectionViewCell.swift
//  SuperHeroes
//
//  Created by Наталья Миронова on 26.05.2023.
//

import UIKit

//MARK: - CollectionViewCell
class CollectionViewCell: UICollectionViewCell {
    
    //MARK: - Property
    static let identifier = "superhero"
    
    private var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .green
        return label
    }()
    
    private var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.layer.cornerRadius = 20
        image.clipsToBounds = true
        return image
    }()
    
    private var imageURL: URL? {
        didSet {
            imageView.image = nil
            updateImage()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(nameLabel)
        contentView.addSubview(imageView)
        contentView.clipsToBounds = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        nameLabel.frame = CGRect(x: 5, y: contentView.frame.size.height-50, width: contentView.frame.size.width-10, height: 50)
        
        imageView.frame = CGRect(x: 5, y: 0, width: contentView.frame.size.width-10, height: contentView.frame.size.height-50)
    }
    
    func configure(with superhero: Superhero) {
        nameLabel.text = superhero.name
        imageURL = URL(string: superhero.images.lg)

    }
    
    private func updateImage() {
        guard let url = imageURL else { return }
        
        getImage(from: url) {[unowned self] result in
            switch result {
            case .success(let image):
                if url == self.imageURL {
                    self.imageView.image = image
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func getImage(from url: URL, completion: @escaping(Result<UIImage, Error>) -> Void) {
        
        if let cachedImager = ImageCache.shared.object(forKey: url.lastPathComponent as NSString) {
            print("Image from cache: \(url.lastPathComponent)")
            completion(.success(cachedImager))
            return
        }
        
        NetworkManager.shared.fetchImage(from: url) { result in
            switch result {
            case .success(let data):
                guard let image = UIImage(data: data) else { return }
                ImageCache.shared.setObject(image, forKey: url.lastPathComponent as NSString)
                print("Image from network: \(url.lastPathComponent)")
                completion(.success(image))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
    
    
