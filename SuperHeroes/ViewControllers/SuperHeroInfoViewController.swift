//
//  SuperHeroInfoViewController.swift
//  SuperHeroes
//
//  Created by Наталья Миронова on 25.05.2023.
//

import UIKit

//MARK: - Super Heroe Info ViewController
class SuperheroInfoViewController: UIViewController {
    
    //MARK: - Property
    var superhero: Superhero?

    private var info = UILabel()
    private var imageHero = UIImageView()
    
    //MARK: - Override UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
}

//MARK: - Setting View
private extension SuperheroInfoViewController {
    func setupView() {
        view.backgroundColor = .black
        
        addSubViews()
        setupLabel()
        setupImage()
        setupLayout()
    }
}

//MARK: - Setting
private extension SuperheroInfoViewController {
    
    func addSubViews() {
        [info, imageHero].forEach { view.addSubview($0)
        }
    }
    
    func setupImage() {
        
        guard let hero = superhero else { return }
        guard let url = URL(string: hero.images.sm) else { return }
        NetworkManager.shared.fetchImage(from: url) { result in
            switch result {
            case .success(let data):
                guard let image = UIImage(data: data) else { return }
                self.imageHero.image = image
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func setupLabel() {
        guard let hero = superhero else { return }
        info.textColor = .green
        info.numberOfLines = 10
        info.text = hero.powerstats.description
        title = hero.name
    }
}

//MARK: - Layout
private extension SuperheroInfoViewController {
    
    func setupLayout() {
        [info, imageHero].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            imageHero.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            imageHero.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            info.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            info.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.4),
            info.topAnchor.constraint(equalTo: imageHero.bottomAnchor, constant: 10),
            info.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}

