//
//  ViewController.swift
//  SuperHeroes
//
//  Created by Наталья Миронова on 24.05.2023.
//

import UIKit

//MARK: - Welcome View Controller
final class WelcomeViewController: UIViewController {
    
    //MARK: - Private property
    private let welcomeLabel = UILabel()
    private let startButton = UIButton()
    
    //MARK: - Override UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK: - Actions
    @objc
    private func touchButton() {
        let superHeroesVC = SuperHeroesViewController()
        let navigationController = UINavigationController(rootViewController: superHeroesVC)
        navigationController.modalPresentationStyle = .fullScreen
        present(navigationController, animated: true)
    }
}

//MARK: - Setting View
private extension WelcomeViewController {
    func setupView() {
        view.backgroundColor = .black
        
        addSubViews()
        addActions()
        
        setupLabel()
        setupButton()
        
        setupLayout()
    }
}

//MARK: - Setting
private extension WelcomeViewController {
    
    func addSubViews() {
        [welcomeLabel, startButton].forEach { view.addSubview($0) }
    }
    
    func addActions() {
        startButton.addTarget(self,
                              action: #selector (touchButton),
                              for: .touchUpInside)
    }
    
    func setupLabel() {
        welcomeLabel.font = .systemFont(ofSize: 50, weight: .bold)
        welcomeLabel.text = "Super Heroes"
        welcomeLabel.textColor = .green
    }
    
    func setupButton() {
        startButton.setTitle("Start", for: .normal)
        startButton.setTitleColor(.black, for: .normal)
        startButton.setTitleColor(.lightGray , for: .highlighted)
        startButton.titleLabel?.font = .systemFont(ofSize: 30)
        startButton.layer.cornerRadius = 10
        startButton.backgroundColor = .green
    }
}

//MARK: - Layout
private extension WelcomeViewController {
    
    func setupLayout() {
        
        [welcomeLabel, startButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            
            welcomeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            welcomeLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            startButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            startButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.08),
            startButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.80)
        ])
    }
}

