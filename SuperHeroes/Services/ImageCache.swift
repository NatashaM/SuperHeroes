//
//  ImageCache.swift
//  SuperHeroes
//
//  Created by Наталья Миронова on 26.05.2023.
//

import UIKit

//MARK: - ImageCache
class ImageCache {
    static let shared = NSCache<NSString, UIImage>()
    
    private init() {}
}
