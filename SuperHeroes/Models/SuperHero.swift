//
//  SuperHeroes.swift
//  SuperHeroes
//
//  Created by Наталья Миронова on 24.05.2023.
//

import Foundation

// MARK: - SuperHero
struct Superhero: Codable {
    let id: Int
    let name, slug: String
    let powerstats: Powerstats
    let appearance: Appearance
    let biography: Biography
    let work: Work
    let connections: Connections
    let images: Images
}

// MARK: - Appearance
struct Appearance: Codable {
    let gender: String
    let race: String?
    let height, weight: [String]
    let eyeColor, hairColor: String
}

// MARK: - Biography
struct Biography: Codable {
    let fullName, alterEgos: String
    let aliases: [String]
    let placeOfBirth, firstAppearance: String
    let publisher: String?
    let alignment: String
}

// MARK: - Connections
struct Connections: Codable {
    let groupAffiliation, relatives: String
}

// MARK: - Images
struct Images: Codable {
    let xs, sm, md, lg: String
}

// MARK: - Powerstats
struct Powerstats: Codable {
    let intelligence, strength, speed, durability: Int
    let power, combat: Int
    
    var description: String {
        """
    Intelligence: \(intelligence)
    Strength: \(strength)
    Speed: \(speed)
    Durability: \(durability)
    Power: \(power)
    Combat: \(combat)
    """
    }
}

// MARK: - Work
struct Work: Codable {
    let occupation, base: String
}



